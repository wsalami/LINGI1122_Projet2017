# LINGI1122_Projet2017

## Deuxième partie

Maintenant que vous avez décortiqué précisément le problème posé, nous vous demandons de fournir
en Dafny une implémentation complète de vos méthodes. Votre code devra être exécutable par dafny, et
sera fourni avec trois tests montrant le bon fonctionnement du programme.
Pour cette phase-ci, il ne faut pas spécifier le programme au delà de ce qui est strictement
nécessaire pour permettre à dafny de le compiler. En pratique, il faudra quand même spécifier que vous
n’accédez pas à des références nulles, que les indices de vos tableaux sont dans les bornes, et que vos
boucles se terminent. Comme nombre d’entre vous l’ont remarqué durant le TP, c’est beaucoup plus facile
de faire accepter un programme à Dafny si on n’écrit pas de postconditions. C’est ce qui est demandé
ici. La preuve complète, sur base des spécifications de la phase 1 fera l’objet de la phase 3.
Votre programme devra rester le plus fidèle possible à la description faite à la phase 1. Si vous déci-
dez de vous en écarter, vous devrez justifier votre décision et montrer pourquoi il n’était pas possible
d’implémenter ce que vous aviez décrit.
En pratique, il faut implémenter une classe Couverture avec un constructeur qui reçoit une liste de
rectangles et une méthode optimize() qui optimise (localement) la liste de rectangles. Toutes les méthodes
de la phase 1 doivent être implémentées dans votre programme, dans la classe Couverture ou en dehors,
à l’exception de la méthode improve, qui n’est que fortement conseillée.
Un schéma de programme vous est fourni pour vous lancer, et vous montrer les notations nécessaires
au projet. Pour pouvoir exécuter le code, vous devrez utiliser l’option “/compile:3” ou utiliser la tâche
INGInious “Execute Dafny”. Rise4Fun ne permet pas d’exécuter le code Dafny, seulement de le vérifier.
Si vous n’arrivez pas à installer Dafny, pensez à demander son installation via le Doodle https://
beta.doodle.com/poll/irmub7gy24k5drvm. Si, comme c’est le cas pour l’instant, trop peu de gens sont
intéressés, nous considèrerons que vous êtes capables de vous débrouiller.
Votre code est votre rapport ! Pensez donc à bien le commenter pour ses futurs lecteurs. En
particulier, toutes vos méthodes et fonctions doivent être accompagnées d’un commentaire décrivant
leur comportement et leur utilité, c’est à dire leur spécification informelle. C’est là que vous devez aussi
justifier les différences entre votre design et votre implémentation. Le fichier “groupeXX.dfy” est à
remettre sur Moodle selon les mêmes modalités que la phase 1.

## Troisième partie

Sans surprise, la troisième partie du projet vise à prouver la validité de vos spécifications (partie 1)
sur votre programme (partie 2). Dans cette partie, il vous est demandé de reprendre le programme de la
partie précédente, et de le compléter afin de démontrer que votre programme répond bien au problème
décrit ci-dessus.
Le résultat attendu est un programme dafny valide. En particulier, toutes les annotations doivent
être vérifiables par dafny. Si vous souhaitez ajouter des annotations que dafny ne peut pas prouver,
transformez-le en commentaire de la manière suivante : // FAIL: ensures 1 == 2 Pour vous aider, une
implémentation d’exemple respectant les exigences de la partie 2 vous est fournie avec ce projet. Vous
pouvez vous en inspirer autant que nécessaire. Si votre implémentation pose trop de problèmes, vous
pouvez éventuellement repartir de cette dernière pour réaliser la partie 3. Dans ce cas, je vous demande
de passer d’abord dans mon bureau pour en discuter.
En plus de votre code, vous devez remettre un rapport de deux pages maximum répondant aux points
ci dessous. La concision est l’objectif principal du rapport. Ne perdez pas de temps à ré-expliquer les
parties connues du projet. Concentrez-vous sur les spécificités de votre programme à vous.
— Quelles sont les changements et corrections majeurs apportés à vos spécifications du point 1. En
quoi sont-ils nécessaires ?
— Quelle technique avez-vous utilisée pour prouver que la méthode optimise ne modifie en rien la
disposition des tuiles couvertes par une Couverture, mais seulement leur représentation interne.
— Énoncez en français la garantie d’optimalité donnée par optimize dans votre programme. Veillez à
être précis mais aussi concis que possible.
— S’il reste de la place, discutez les assertions que vous n’êtes pas parvenus à prouver. Discutez aussi
les garanties offertes par la preuve sur la qualité du programme.
Vous devez remettre une unique archive contenant votre code et votre rapport sur Moodle pour le
mercredi 10 mai 2017 à 12h00, toujours selon le principe d’une seule soumission par groupe. La remise
des travaux sera ouverte jusqu’à 14h00 (avec une pénalité de 10%) et jusqu’à 16h00 (avec une pénalité
de 20%). L’échéance pour la correction croisée a été adaptée en conséquence.
