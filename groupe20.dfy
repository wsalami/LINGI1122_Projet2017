/**
* Projet MCP - Partie 2
*   by Denauw Antoine 18371400
*   by Salame William 50691400
*   by De Carvalho Borges Marcio Antonio 42851400
*
*/


// Le type de données pour les rectangles et les positions
datatype Rectangle = Rectangle(position: Position, vertical: int, horizontal: int)
datatype Position = Position(x: int,y: int)

/* OK */
predicate method okPos(p: Position)
{
  p.x >= 0 && p.y >= 0
}

predicate method okRect(t: Rectangle)
{
  okPos(t.position) && t.vertical > 0 && t.horizontal > 0
}

//L'abstraction d'une position est elle meme
function method absPos(p: Position): Position
{
  p
}
//Contains Bis
predicate method containsPos(q: Rectangle, p: Position)
{ q.position.x <= p.x < q.position.x + q.horizontal && q.position.y <= p.y < q.position.y + q.vertical }

function method absRect(q: Rectangle): set<Position>
{
  set x, y | q.position.x <= x < q.position.x + q.horizontal && q.position.y <= y < q.position.y + q.vertical :: Position.Position(x, y)
}
//Retourne true si les retcangles peuvent merge
predicate method canMerge(a: Rectangle, b: Rectangle)
{
    (a.vertical == b.vertical && a.position.y == b.position.y && ((a.position.x + a.horizontal == b.position.x) || (b.position.x + b.horizontal == a.position.x)))
    || (a.horizontal == b.horizontal && a.position.x == b.position.x && ((a.position.y + a.vertical == b.position.y) || (b.position.y + b.vertical == a.position.y)))
}
//Retourne true si la position est contenue dans un rectangle
predicate method contains(x: int, y: int,rect: Rectangle)
{
  rect.position.x <= x < rect.position.x + rect.horizontal && rect.position.y <= y < rect.position.y + rect.vertical
}
//return true si aucune position ne se superpose entre deux rectangles
predicate method superpose(rect1: Rectangle, rect2 : Rectangle)
{
  forall x,y | rect1.position.x <= x < rect1.position.x + rect1.horizontal && rect1.position.y <= y < rect1.position.y + rect1.vertical :: !contains(x,y,rect2)
}

method merge(rect1: Rectangle, rect2: Rectangle) returns (rect: Rectangle)
requires okRect(rect1) && okRect(rect2) && canMerge(rect1,rect2) && superpose(rect1,rect2)
ensures okRect(rect)
ensures forall x,y | contains(x,y,rect1) :: contains(x,y,rect)
ensures forall x,y | contains(x,y,rect2) :: contains(x,y,rect)
ensures forall x,y | contains(x,y,rect) :: (contains(x,y,rect1) || contains(x,y,rect2))
{
  //Si ils sont allignés en X
  if(rect1.position.x == rect2.position.x){
    var smallY;
    //On récupère la plus petite position en Y pour construire le nouveau Rectangle
    if(rect1.position.y < rect2.position.y) {smallY := rect1.position.y;}
    else { smallY := rect2.position.y;}
    //On construit le Rectangle résultant
    rect := Rectangle(Position(rect1.position.x,smallY),rect1.vertical + rect2.vertical,rect1.horizontal);
  }
  //Si ils sont allignés en Y
  else {
    var smallX;
    //On récupère la plus petite position en X pour construire le nouveau Rectangle
    if(rect1.position.x < rect2.position.x) { smallX := rect1.position.x;}
    else { smallX := rect2.position.x;}
    //On construit le Rectangle résultant
    rect := Rectangle(Position(smallX,rect1.position.y),rect1.vertical,rect1.horizontal + rect2.horizontal);
  }

}

class Couverture {

  var rectArray : array<Rectangle>; // Tableau qui contiendra les Rectangles
  var size : int; //Variable nous disant le nombre d'élément dans la couverture actuelle



   predicate method ok()
   reads this, rectArray
   {
     rectArray != null &&
     0 <= size <= rectArray.Length &&
     forall i | 0 <= i < size :: okRect(rectArray[i]) &&
     forall i,j | 0 <= i < size && 0 <= j < size :: superpose(rectArray[i],rectArray[j])
   }

   //True si la position est dans la couverture
   predicate method containsCouv(x: int,y: int)
   requires ok()
   reads this,rectArray
   {
     exists i | 0 <= i < size :: contains(x,y,rectArray[i])
   }

   function method absCouv(): set<Position>
   requires ok()
   reads this,rectArray
   ensures ok()
   {
     set i,e | 0 <= i < size && e in absRect(rectArray[i]) :: e
   }

   constructor (qs: array<Rectangle>)
   requires qs != null && qs.Length > 0 &&  forall i | 0 <= i < qs.Length :: okRect(qs[i])
   requires forall i,j | 0 <= i < qs.Length && 0 <= j < qs.Length :: superpose(qs[i],qs[j])
   modifies this
   ensures ok() && rectArray == qs
   {
     rectArray := qs;
     size := qs.Length;
   }

  method improve() returns (res:bool)
   modifies this,rectArray
   requires ok()
   ensures ok()
   ensures rectArray == old (rectArray)  //Post : On ne modifie pas l'addresse du tableau
   ensures size <= old(size) //POST: la taille pourrait diminuer
   ensures res <==> size < old(size) //POST : si la taille diminue alors res est true et inversement
   ensures forall x,y ::  containsCouv(x,y) <==> old(containsCouv(x,y))//POST: la couverture couvre les mêmes positions qu'au début
   ensures size == old(size) ==> forall i,j | 0 <= i < size && 0 <= j < size :: !canMerge(rectArray[i],rectArray[j]) //POST: si la taille est la même alors on ne peut plus merge
   ensures !res ==> forall x | 0 <= x < size :: rectArray[x] == old(rectArray[x]) //Si on ne peut rien merge alors le tableau n'est pas modifie
   {

     var i := 0;
     var j := 0;
     res := false;
     while i < size
     decreases old(size) - i
     invariant ok()
     invariant rectArray == old(rectArray)  //Post : On ne modifie pas l'addresse du tableau
     invariant 0 <= i <= size <= old(size)
     invariant forall x,y ::  containsCouv(x,y) <==> old(containsCouv(x,y)) //la couverture couvre les mêmes positions qu'au début
     invariant size == old(size) ==> forall x,y | 0 <= x < i && 0 <= y < size :: !canMerge(rectArray[x],rectArray[y]) //Si l'on passe un rectangle alors on n'a pas trouve de merge faisable avec celui-ci
     {
       assert ok();
       j := 0;
       while j < size
         decreases size - j
         invariant ok()
         invariant rectArray == old(rectArray)  //Post : On ne modifie pas l'addresse du tableau
         invariant 0 <= j <= size <= old(size)
         invariant 0 <= i < size <= old(size)
         invariant forall x,y ::  containsCouv(x,y) <==> old(containsCouv(x,y))
         invariant size == old(size) ==> forall x,y | 0 <= x < i && 0 <= y < size :: !canMerge(rectArray[x],rectArray[y])  //Meme qu'en haut
         invariant size == old(size) ==> forall y |  0 <= y < j :: !canMerge(rectArray[i],rectArray[y]) //Si l'on passe un rectangle alors on n'a pas trouve de merge faisable avec celui-ci
       {
         var boolean := false;
         if(canMerge(rectArray[i],rectArray[j])){
           fusion(i,j);
           return true;
         }
         j := j + 1;
       }
       i := i +1;
     }
   }

   method fusion(i: int,j: int)
   modifies this,rectArray
   requires ok()
   requires 0 <= i < size //PRE: i et j doivent être entre 0 et size-1
   requires 0 <= j < size
   requires canMerge(rectArray[i],rectArray[j]) //PRE: les rectangles à l'indice i et j peuvent être fusionnés
   ensures ok()
   ensures rectArray == old(rectArray) //Post : On ne modifie pas l'addresse du tableau
   ensures size < old(size) //POST: suite à la fusion de deux rectangles, la taille diminue
   ensures forall x,y :: containsCouv(x,y) <==> old(containsCouv(x,y)) //POST: la couverture couvre les mêmes positions qu'au début
   ensures forall x,y :: contains(x,y,rectArray[i]) <==> (contains(x,y,old(rectArray[i])) || contains(x,y,old(rectArray[j]))) //POST : le rectangle a la position i doit contenir toutes les positions des anciens rectangles aux anciennes positions i et j et inversement
   ensures forall x,y :: contains(x,y,old(rectArray[size-1])) <==> contains(x,y,rectArray[j]) //POST : le rectangle a la position j doit contenir toutes les positions de l'ancien rectangle en bout de tableau et inversement
   {
     var rect := merge(rectArray[i],rectArray[j]);
     //On place d'abord le Rectangle résultant en i
     rectArray[i] := rect;
     //On place ensuite le dernier élément de la liste en j pour avoir les Rectangles courant dans les size premiers
     rectArray[j] := rectArray[size-1];
     if(size > 0) {size := size - 1;}
   }

  method optimize()
  modifies this ,rectArray
  requires ok()
  ensures rectArray == old(rectArray)
  ensures ok()
  ensures size <= old(size)
  ensures forall i,j | 0 <= i < size && 0 <= j < size :: !canMerge(rectArray[i],rectArray[j]) //POST: on ne peut plus merge
  ensures forall x,y :: containsCouv(x,y) <==> old(containsCouv(x,y)) //POST: la couverture couvre les mêmes positions qu'au début
  {
    var changed := true;
    //Tant que l'appel à improve optimise la couverture
    while changed
    invariant rectArray == old(rectArray)
    invariant ok()
    invariant forall x,y :: containsCouv(x,y) <==> old(containsCouv(x,y))// La couverture couvre les mêmes positions qu'au début
    invariant changed || forall i,j | 0 <= i < size && 0 <= j < size :: !canMerge(rectArray[i],rectArray[j]) //Soit il y a eu une optimisation, soit on ne peut plus rien merge
    invariant size == old(size) ==> forall x | 0 <= x < size :: rectArray[x] == old(rectArray[x]) //Si on ne peut rien merge alors le tableau n'est pas modifie
    decreases size, changed
    {
      changed := improve();
    }
  }
}
