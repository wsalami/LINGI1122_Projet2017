/**
* Projet MCP - Partie 2
*   by Denauw Antoine 18371400
*   by Salame William 50691400
*   by De Carvalho Borges Marcio Antonio 42851400
*
*/


// Le type de données pour les rectangles et les positions
datatype Rectangle = Rectangle(position: Position, vertical: int, horizontal: int)
datatype Position = Position(x: int,y: int)

/* OK */
predicate method okPos(p: Position)
{
  p.x >= 0 && p.y >= 0
}

predicate method okRect(t: Rectangle)
{
  okPos(t.position) && t.vertical > 0 && t.horizontal > 0
}

/* ABS */
//Les abstractions sont laissées vides pour la partie III
function method absPos(p: Position): Position
{
  p
}

function method absRect(t: Rectangle): int
{
  1
}

class Couverture {

  var rectArray : array<Rectangle>; // Tableau qui contiendra les Rectangles
  var size : int; //Variable nous disant le nombre d'élément dans la couverture actuelle

  predicate ok()
  reads this, rectArray
  {
    rectArray != null && 0 < size <= rectArray.Length && forall i | 0 <= i < size :: okRect(rectArray[i])
  }

  constructor (qs: array<Rectangle>)
  requires qs != null && qs.Length > 0 &&  forall i | 0 <= i < qs.Length :: okRect(qs[i])
  modifies this
  ensures ok() && rectArray == qs
  {
    rectArray := qs;
    size := qs.Length;
  }

  method contains(x: int, y: int) returns (rect: Rectangle)
  requires ok()
  {
    var i := 0;
    var pos := Position(x,y); // Construction de la Position recherchée
    while i < size // Tant que nous avons pas parcouru tout les rectangles restant
    {
      if(pos == rectArray[i].position) // Si le rectangle a la position recherchée
      {
        return rectArray[i];
      }
      i := i + 1;
    }

  }

  method canMerge(rect1 : Rectangle, rect2 : Rectangle) returns (res: bool)
  {
    //Si les deux Rectangles ne sont allignés ni sur l'abscisse ni sur l'ordonnée
    if(rect1.position.x != rect2.position.x && rect1.position.y != rect2.position.y) { return false; }

    res := false;
    var close := false;

    //Si ils sont sur la même abscisse
    if(rect1.position.x == rect2.position.x) {
      //Si le Rectangle 1 est en dessous du Rectangle 2 et que les deux Rectangles sont adjacents
      if(rect1.position.y < rect2.position.y && rect1.position.y + rect1.vertical == rect2.position.y) {
        close := true;
      }
      //Si le Rectangle 2 est en dessous du Rectangle 1 et que les deux Rectangles sont adjacents
      if(rect2.position.y < rect1.position.y && rect2.position.y + rect2.vertical == rect1.position.y) {
        close := true;
      }
      //Retourne true si les Rectangles sont adjacents et que le côté adjacent des deux rectangles soit de même longueur
      return close && (rect1.horizontal == rect2.horizontal);
    }
    //Si ils sont sur la même ordonnée
    else {
      //Si le Rectangle 1 est à gauche du Rectangle 2 et que les deux Rectangles sont adjacents
      if(rect1.position.x < rect2.position.x && rect1.position.x + rect1.horizontal == rect2.position.x) {
        close := true;
      }
      //Si le Rectangle 2 est à gauche du Rectangle 1 et que les deux Rectangles sont adjacents
      if(rect2.position.x < rect1.position.x && rect2.position.x + rect2.horizontal == rect1.position.x) {
        close := true;
      }
      //Retourne true si les Rectangles sont adjacents et que le côté adjacent des deux rectangles soit de même longueur
      return close && (rect1.vertical == rect2.vertical);
    }
    //Retourne false si les Rectangles ne sont pas allignés (ni en x ni en y)
    return res;
  }

  method merge(rect1: Rectangle, rect2: Rectangle) returns (rect: Rectangle)
  {
    //Si ils sont allignés en X
    if(rect1.position.x == rect2.position.x){
      var smallY;
      //On récupère la plus petite position en Y pour construire le nouveau Rectangle
      if(rect1.position.y < rect2.position.y) {smallY := rect1.position.y;}
      else { smallY := rect2.position.y;}
      //On construit le Rectangle résultant
      rect := Rectangle(Position(rect1.position.x,smallY),rect1.vertical + rect2.vertical,rect1.horizontal);
    }
    //Si ils sont allignés en Y
    else {
      var smallX;
      //On récupère la plus petite position en X pour construire le nouveau Rectangle
      if(rect1.position.x < rect2.position.x) { smallX := rect1.position.x;}
      else { smallX := rect2.position.x;}
      //On construit le Rectangle résultant
      rect := Rectangle(Position(smallX,rect1.position.y),rect1.vertical,rect1.horizontal + rect2.horizontal);
    }

  }

  method improve() returns (res:bool)
  modifies rectArray
  ensures rectArray == old (rectArray)
  {
    //Si on nous demande de improve une liste non conforme
    if(rectArray == null || size < 1 || size > rectArray.Length){return;}

    var i := 0;
    var j := 0;
    res := false;
    var max := size;

    while i < max
    modifies rectArray
    decreases max - i
    invariant 0 <= i <= max
    {
      j := 0;
      while j < max
      modifies rectArray
      decreases max - j
      invariant 0 <= j <= max
      {
        var boolean := false;
        //On regarde si les deux rectangles courant peuvent être fusionnés
        boolean := canMerge(rectArray[i],rectArray[j]);
        //Si oui
        if(boolean == true){
          //Fuuusion
          var rect := merge(rectArray[i],rectArray[j]);
          //On place d'abord le Rectangle résultant en i
          rectArray[i] := rect;
          //On place ensuite le dernier élément de la liste en j pour avoir les Rectangles courant dans les size premiers
          rectArray[j] := rectArray[max-1];
          max := max - 1;
          return true;
        }
        j := j + 1;
      }
      i := i +1;
    }
  }

  method optimize()
  modifies this , rectArray
  decreases *
  requires ok()
  ensures rectArray == old(rectArray)
  {
    var changed := true;
    //Tant que l'appel à improve optimise la couverture
    while changed
    invariant rectArray == old(rectArray)
    decreases *;
    {
      changed := improve();
      //Si on a improve la couverture on diminue la taille dû à la fusion
      if(changed) {size := size - 1;}
    }
  }

  /* Print le tableau de Rectangle */
  method dump()
  requires rectArray != null
  {
    var i := 0;
    var first := true;
    print "[ ";
    while i < rectArray.Length
    {
      if !first { print ", "; }
      print rectArray[i];
      i := i + 1;
      first := false;
    }
    print " ]\n";
  }
}

method Main()
decreases *
{
  //===================== FirstTest =====================/

  var g := new Rectangle[3];

  g[0] := Rectangle(Position(0,0), 1, 1); //Position (x,y), vertical, horizontal
  g[1] := Rectangle(Position(0,1), 1, 1);
  g[2] := Rectangle(Position(1,0), 2, 1);

  var m := new Couverture(g);
  if(m == null){return;}
  print "===== Test n°1 ===== \nCouverture de base: \n";
  m.dump();
  m.optimize();
  print "Couverture optimale: \n";
  m.dump();
    
  print"La couverture est représentées par les ";
  print m.size;
  print " premiers éléments";
  print "\n\n";

  var result1 := Rectangle(Position(0,0), 2, 2);

  //===================== 2ndTest =====================/
  var h := new Rectangle[3];

  h[0] := Rectangle(Position(0,0), 1, 1); //position (x,y), vertical, horizontal
  h[1] := Rectangle(Position(1,0), 1, 1);
  h[2] := Rectangle(Position(1,1), 1, 1);

  var n := new Couverture(h);
  print "===== Test n°2 ===== \nCouverture de base: \n";
  n.dump();
  n.optimize();
  print "Couverture optimale: \n";
  n.dump();
    
  print"La couverture optimale est représentées par les ";
  print n.size;
  print " premiers éléments";
  print "\n\n";

  var result2 := new Rectangle[2];
  result2[0] := Rectangle(Position(0,0), 1, 2);
  result2[1] := Rectangle(Position(1,1), 1, 1);

  //===================== 3rdTest =====================/
  var i := new Rectangle[4];

  i[0] := Rectangle(Position(0,0), 1, 1); //position (x,y), vertical, horizontal
  i[1] := Rectangle(Position(0,1), 2, 1);
  i[2] := Rectangle(Position(1,2), 1, 2);
  i[3] := Rectangle(Position(3,1), 1, 1);

  var o := new Couverture(i);
  print "===== Test n°3 ===== \nCouverture de base: \n";
  o.dump();
  o.optimize();
  
  print "Couverture optimale: \n";
  o.dump();
  
  print"La couverture optimale est représentées par les ";
  print o.size;
  print " premiers éléments";
  print "\n\n";

  //===================== 4thTest =====================/
  var z := new Rectangle[7];
  z[0] := Rectangle(Position(0,0), 2, 2);
  z[1] := Rectangle(Position(0,2), 2, 2);
  z[2] := Rectangle(Position(0,4), 1, 2);
  z[3] := Rectangle(Position(2,1), 2, 2);
  z[4] := Rectangle(Position(4,1), 2, 1);
  z[5] := Rectangle(Position(2,3), 2, 1);
  z[6] := Rectangle(Position(3,3), 2, 2);

  var x := new Couverture(z);
  print "===== Test n°4 ===== \nCouverture de base: \n";
  x.dump();
  x.optimize();
  print "Couverture optimale: \n";
  x.dump();
    
  print"La couverture optimale est représentées par les ";
  print x.size;
  print " premiers éléments";
  print "\n\n";
}
